**Projeto Teste Backend**

## tarefa1 - arquivos referentes a primeira tarefa
- show_create_table.sql - arquivo contendo o resultado do comando SHOW CREATE TABLE

---

## tarefa2 - arquivos referentes a segunda tarefa
- Estrutura de Arquivos
   - api/
      - Pmweb_Orders_Stats.php - Classe da API
   - database/
      - Collections.php - arquivo responsável por operações com o banco de dados
      - config.php - arquivo com as informações para conexão com o banco de dados
      - DatabaseManager.php - arquivo responsável pelo gerenciamento da conexão com o banco de dados
   - index.php

---

## tarefa3 - arquivos referentes a terceira tarefa
- Estrutura de Arquivos
   - api/
      - Pmweb_Orders_Stats.php - Classe da API
   - database/
      - Collections.php - arquivo responsável por operações com o banco de dados
      - config.php - arquivo com as informações para conexão com o banco de dados
      - DatabaseManager.php - arquivo responsável pelo gerenciamento da conexão com o banco de dados
   - index.php - arquivo com RestServer
   - request.php - arquivo com exemplo de requisição HTTP para a API
