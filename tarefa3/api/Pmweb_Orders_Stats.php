<?php

include_once 'database/DatabaseManager.php';
include_once 'database/Collections.php';

/**
* Sumarizações de dados transacionais de pedidos.
*/
class Pmweb_Orders_Stats
{
	protected $conditions = []; // Condições para seleção de objetos do banco

	/**
	* Testa se a data atende o formato indicado (Y-m-d)
	* @param String $date data informada
	** @return boolean
	*/
	public function checkDate($date)
	{
		return (bool)preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date);
	}

	/**
	* Define o período inicial da consulta.
	* @param String $date Data de início, formato `Y-m-d` (ex, 2017-08-24).
	** @return void
	*/
	public function setStartDate($date)
	{
		if(!$this->checkDate($date))
		{
			throw new Exception("Formato de data inicio inválido!");
		}

		// Posição 0 - coluna da tabela
        // Posição 1 - operador para comparação (>,<,=)
        // Posição 2 - valor que será comparado
        // Posição 3 - operador lógico (AND, OR) - OPCIONAL
		$this->conditions['start'] = array('order_date', '>=', $date);
	}

	/**
	* Define o período final da consulta.
	** @param String $date Data final da consulta, formato `Y-m-d` (ex, 2017-08-24).
	** @return void
	*/
	public function setEndDate($date)
	{
		if(!$this->checkDate($date))
		{
			throw new Exception("Formato de data final inválido!");
		}

		// Posição 0 - coluna da tabela
        // Posição 1 - operador para comparação (>,<,=)
        // Posição 2 - valor que será comparado
        // Posição 3 - operador lógico (AND, OR) - OPCIONAL
		$this->conditions['end'] = array('order_date', '<=', $date);
	}

	/**
	* Retorna o total de pedidos efetuados no período.
	** @return integer Total de pedidos.
	*/
	public function getOrdersCount()
	{
		$collection = new Collections('order_item');
		return intval($collection->criteria($this->conditions)->countDistinct('order_id'));
	}

	/**
	* Retorna a receita total de pedidos efetuados no período.
	** @return float Receita total no período.
	*/
	public function getOrdersRevenue()
	{
		$collection = new Collections('order_item');
		return floatval($collection->criteria($this->conditions)->sumBy('round(quantity * price, 2)'));
	}

	/**
	* Retorna o total de produtos vendidos no período (soma de quantidades).
	** @return integer Total de produtos vendidos.
	*/
	public function getOrdersQuantity()
	{
		$collection = new Collections('order_item');
		return intval($collection->criteria($this->conditions)->sumBy('quantity'));
	}

	/**
	* Retorna o preço médio de vendas (receita / quantidade de produtos).
	** @return float Preço médio de venda.
	*/
	public function getOrdersRetailPrice()
	{
		$quantidade = $this->getOrdersQuantity();
		$receita = $this->getOrdersRevenue();

		if($quantidade == 0)
		{
			return 0;
		}

		return round($receita / $quantidade, 2);
	}

	/**
	* Retorna o ticket médio de venda (receita / total de pedidos).
	** @return float Ticket médio.
	*/
	public function getOrdersAverageOrderValue()
	{
		$total = $this->getOrdersCount();
		$receita = $this->getOrdersRevenue();

		if($total == 0)
		{
			return 0;
		}

		return round($receita / $total, 2);
	}

	/**
	* Retorna estatísticas.
	** @return array Estatísticas dos pedidos.
	*/
	public function getStatistics()
	{
		$dbinfo = include('database/config.php');

		DatabaseManager::open($dbinfo);

		$quantidade = $this->getOrdersQuantity();
		$total = $this->getOrdersCount();
		$receita = $this->getOrdersRevenue();

		$preco_medio = 0;
		if($quantidade > 0)
		{
			$preco_medio = round($receita / $quantidade, 2);
		}

		$ticket_medio = 0;
		if($total > 0)
		{
			$ticket_medio = round($receita / $total, 2);
		}

		$response = [
			"status" => 200,
			"message" =>
			[
				"orders" => [
					"count" => $total,
					"revenue" => $receita,
					"quantity" => $quantidade,
					"averageRetailPrice" => $preco_medio,
					"averageOrderValue" => $ticket_medio
				]
			]
		];

		DatabaseManager::close();

		return $response;
	}
}
?>