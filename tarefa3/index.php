<?php
include_once 'api/Pmweb_Orders_Stats.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

class RestServer
{
    public static function run()
    {
        $startDate   = isset($_GET['startDate']) ? $_GET['startDate'] : '';
        $endDate   = isset($_GET['endDate']) ? $_GET['endDate'] : '';
        $response = NULL;

        try
        {
            $api = new Pmweb_Orders_Stats;
            $api->setStartDate($startDate);
            $api->setEndDate($endDate);

            $response = $api->getStatistics();
        }
        catch (Exception $e)
        {
            $response = array('status' => 400, 'message' => $e->getMessage());
        }
        catch (Error $e)
        {
            $response = array('status' => 400, 'message' => $e->getMessage());
        }

        if (isset($response) && isset($response['status']))
        {
            http_response_code($response['status']);
        }

        if (isset($response) && isset($response['message']))
        {
            $response = $response['message'];
        }

        print json_encode($response);
    }
}

RestServer::run();
