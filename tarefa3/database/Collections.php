<?php

/**
 * Classe responsável por operações com o banco de dados
 */
class Collections
{
    protected $table; // nome da tabela que será acessada
    protected $criteria; // array de critérios para busca no banco

    /**
     * Class Constructor
     * @param $table  nome da tabela que será acessada
     */
    public function __construct($table)
    {
        $this->table = $table;        
        $this->criteria = [];
    }

    /**
     * Adiciona critério
     * 
     * @param  $conditions  array com condições para busca
     * @return Objeto Collections
     */
    public function criteria($conditions = [])
    {
        $this->criteria = $conditions;

        return $this;
    }

     /**
     * Processa critério
     * 
     * @param  $limit  limite de objetos retornados na busca
     * @return Objeto Collections
     */
    protected function getCriteria()
    {
        $criteria = "";
        $values = [];
        if (sizeof($this->criteria) > 0)
        {
            $criteriaAND = [];
            $criteriaOR = [];
            $valuesAND = [];
            $valuesOR = [];

            foreach ($this->criteria as $condition)
            {
                $column        = $condition[0]; // coluna da tabela
                $operator      = $condition[1]; // operador para comparação (>,<,=)
                $value         = $condition[2]; // valor que será comparado
                $logicOperator = sizeof($condition) == 4? $condition[3] : 'AND'; // OPCIONAL - operador lógico (AND, OR)

                $value = $this->transformValue($value); // transforma valor de acordo com o seu tipo antes de enviar para o banco

                if($logicOperator == 'AND')
                {
                    $criteriaAND[] = "{$column} {$operator} ?";
                    $valuesAND[] = $value;
                }
                else
                {
                    $criteriaOR[] = "{$column} {$operator} ?";
                    $valuesOR[] = $value;
                }
            }

            $criteriaAND = implode(" AND ", $criteriaAND);
            $criteriaOR = implode(" OR ", $criteriaOR);

            if (strlen($criteriaAND) > 0 && strlen($criteriaOR) > 0)
            {
                $criteria = "{$criteriaAND} AND ({$criteriaOR})";
            }
            elseif (strlen($criteriaAND) > 0)
            {
                $criteria = "{$criteriaAND}";
            }
            elseif (strlen($criteriaOR) > 0)
            {
                $criteria = "{$criteriaOR}";
            }

            $values = array_merge($valuesAND, $valuesOR);
        }

        return array($criteria, $values);
    }

    /**
     * Agrega coluna
     * @param $function  função de agregação (count, sum, min, max, avg)
     * @param $column    coluna da tabela
     * @param $alias     alias para coluna da tabela (OPCIONAL)
     * @return          Array de objetos ou valor total
     */
    protected function aggregate($function, $column, $alias = null)
    {
        $alias = $alias ? $alias : $column;
        $where = $this->getCriteria();

        $sql = "SELECT $function({$column}) as \"{$alias}\" FROM {$this->table} ";

        if (strlen($where[0]) > 0)
        {
            $sql .= "WHERE {$where[0]} ";
        }
        
        // get the connection of the active transaction
        if ($conn = DatabaseManager::get())
        {
            $result = $conn-> prepare ( $sql , array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result-> execute ( $where[1] );
            
            $results = [];
            
            if ($result)
            {
                // iterate the results as objects
                while ($raw = $result-> fetchObject())
                {
                    $results[] = $raw;
                }
            }
            
            if ($results)
            {
                if ( (count($results) > 1) )
                {
                    return $results;
                }
                else
                {
                    return $results[0]->$alias;
                }
            }
            
            return 0;
        }
        else
        {
            // if there's no active transaction opened
            throw new Exception(AdiantiCoreTranslator::translate('No active transactions') . ': ' . __METHOD__ .' '. $this->getEntity());
        }
    }

    /**
     * Agregação SUM
     * @param $column  coluna que será agregada
     * @param $alias   alias da coluna
     * @return         Array de objetos ou valor total
     */
    public function sumBy($column, $alias = null)
    {
        return $this->aggregate('sum', $column, $alias);
    }

    /**
     * Agregação COUNT DISTINCT 
     * @param $column  coluna que será agregada
     * @param $alias   alias da coluna
     * @return         Array de objetos ou valor total
     */
    public function countDistinct($column, $alias = null)
    {
        $alias = is_null($alias) ? $column : $alias;
        return $this->aggregate('count', 'distinct ' . $column, $alias);
    }

    /**
     * Transforma valor de acordo com o seu tipo antes de enviar para o banco
     * @param $value    Valor a ser transformado
     * @return       Valor transformado
     */
    public function transformValue($value)
    {
        // Se valor for String
        if (is_string($value))
        {
            // add quotes
            $result = htmlspecialchars(strip_tags($value));
        }
        // Se valor for nulo
        else if (is_null($value))
        {
            $result = 'NULL';
        }
        // Se valor for boolean
        else if (is_bool($value))
        {
            $result = $value ? 'TRUE' : 'FALSE';
        }
        else
        {
            $result = $value;
        }
        
        return $result;
    }
}
