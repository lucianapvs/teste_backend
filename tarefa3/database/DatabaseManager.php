<?php

/**
 * Gerenciador de conexão com o banco
 */
class DatabaseManager
{
    private static $conn;     // conexão ativa
    
    /**
     * Construtor da classe
     */
    private function __construct(){}
    
    /**
     * Abre uma conexão e inicia a transação
     * @param $dbinfo Array com informações do banco de dados
     */
    public static function open($dbinfo)
    {
        // lendo propriedades do banco de dados
        $host  = isset($dbinfo['DATABASE_HOSTNAME']) ? $dbinfo['DATABASE_HOSTNAME'] : NULL;
        $user  = isset($dbinfo['DATABASE_USERNAME']) ? $dbinfo['DATABASE_USERNAME'] : NULL;
        $pass  = isset($dbinfo['DATABASE_PASSWORD']) ? $dbinfo['DATABASE_PASSWORD'] : NULL;
        $name  = isset($dbinfo['DATABASE_DATABASE']) ? $dbinfo['DATABASE_DATABASE'] : NULL;
        $port  = isset($dbinfo['DATABASE_PORT']) ? $dbinfo['DATABASE_PORT'] : '3306';

        // instanciando banco
        self::$conn = new PDO("mysql:host={$host};dbname={$name};", $user, $pass);

        // definindo o modo que será usado para reportar erros
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        // iniciando transação
        self::$conn->beginTransaction();
        
        return self::$conn;
    }
    
    /**
     * Retorna a atual conexão ativa
     * @return PDO
     */
    public static function get()
    {
        if (isset(self::$conn))
        {
            return self::$conn;
        }
    }
    
    /**
     * Reverte todas as operações pendentes
     */
    public static function rollback()
    {
        if (isset(self::$conn))
        {
            $driver = self::$conn->getAttribute(PDO::ATTR_DRIVER_NAME);

            // rollback
            self::$conn->rollBack();

            self::$conn = NULL;
            
            return true;
        }
    }
    
    /**
     * Persiste todas as operações pendentes
     */
    public static function close()
    {
        if (isset(self::$conn))
        {
            $driver = self::$conn->getAttribute(PDO::ATTR_DRIVER_NAME);
            
            // aplica todas as operações pendentes
            self::$conn->commit();
            
            self::$conn = NULL;
            
            return true;
        }
    }
}
