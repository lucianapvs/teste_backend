<?php
echo "Teste Backend<br>";

include_once 'database/DatabaseManager.php';
include_once 'database/Collections.php';
include_once 'api/Pmweb_Orders_Stats.php';

try 
{
	$dbinfo = include('database/config.php');

	DatabaseManager::open($dbinfo);

	$api = new Pmweb_Orders_Stats;
	$api->setStartDate('2014-01-01');
	$api->setEndDate('2014-08-30');

	$quantidade = $api->getOrdersQuantity();
	$total = $api->getOrdersCount();
	$receita = $api->getOrdersRevenue();

	$preco_medio = $api->getOrdersRetailPrice();
	$ticket_medio = $api->getOrdersAverageOrderValue();

	echo "Quantidade de produtos vendidos: " . $quantidade . "<br>";
	echo "Total de pedidos: " . $total . "<br>";
	echo "Receita total de pedidos: " . $receita . "<br>";
	echo "Preço médio dos produtos: " . $preco_medio . "<br>";
	echo "Ticket médio de vendas: " . $ticket_medio . "<br>";

	DatabaseManager::close();
} catch (Exception $e) {
	echo $e->getMessage();
}

?>
